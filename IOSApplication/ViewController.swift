//
//  ViewController.swift
//  IOSApplication
//
//  Created by Umihana Spahic on 5/12/19.
//  Copyright © 2019 Umihana Spahic. All rights reserved.
//

import UIKit
import CoreData
import Foundation

var people : [String] = []

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var viewController: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        people.removeAll()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult> (entityName: "User")
        request.returnsObjectsAsFaults = false
        do{
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject]{
                    if let name = result.value(forKey: "name") as? String {
                        people.append(name)
                    }
                }
            }
        }
        catch{
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete{
            people.remove(at : indexPath.row)
            viewController.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = people[indexPath.row]
        return (cell)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        viewController.reloadData()
    }
}

