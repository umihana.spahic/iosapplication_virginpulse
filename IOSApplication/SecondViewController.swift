//
//  SecondViewController.swift
//  IOSApplication
//
//  Created by Umihana Spahic on 5/12/19.
//  Copyright © 2019 Umihana Spahic. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SecondViewController: UIViewController{
    @IBOutlet weak var inputName: UITextField!
    @IBOutlet weak var inputSurname: UITextField!
    var container : NSPersistentContainer!

    
    @IBAction func addItem(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let newName = NSEntityDescription.insertNewObject(forEntityName: "User", into: context)
        if(inputName.text != "" && inputSurname.text != ""){
            let temp = inputName.text! + " " + inputSurname.text!
            newName.setValue(temp,forKey: "name")
            do{
                try context.save()
                print("Saved!")
            }
            catch{
                print("Error")
            }
            inputName.text = ""
            inputSurname.text = ""
        }
        else{
            print("You must put something in the text field")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
