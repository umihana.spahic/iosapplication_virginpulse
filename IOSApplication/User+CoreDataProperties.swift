//
//  User+CoreDataProperties.swift
//  IOSApplication
//
//  Created by Umihana Spahic on 5/13/19.
//  Copyright © 2019 Umihana Spahic. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var name: String

}
